##Description
This script was written to randomize assignment of parcels of land for example after purchasing
land as a group. 

### Motivation
In Kenya, it is common for people to pool resources and buy land in groups to benefit from competitive prices and reduce logistical costs associated with land acquisition. This collaborative approach allows buyers to negotiate better deals, especially in areas with rapidly appreciating land values. However, one of the challenges of group land purchases is ensuring fairness in the allocation of individual parcels, given the differences in size, location, and value of each plot.

This tool is designed to randomly assign parcels of land to individuals in a way that ensures transparency and fairness. By using a randomized process, every participant in the group purchase is given an equal opportunity to secure a piece of land, eliminating potential biases or disputes. This method also helps build trust within the group, as the allocation process is impartial and free from human intervention.

In a country where group land purchases are a growing trend, particularly among middle-income earners looking to invest in real estate, this tool provides an efficient solution to the complex issue of land distribution, making it easier for groups to divide land equitably without conflicts.

N.B Name similarity are purely incidental!

### How to Use
> visit [Demo page](https://qymspace.gitlab.io/randomlandassignment)

![image](/screenshot.png)

### License
 License under GPL [LICENSE](/LICENSE.md)